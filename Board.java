public class Board 
{
    private Square[][] tictactoeBoard;
	
	//Constructor 
	public Board() 
	{
        tictactoeBoard = new Square[3][3];
        for (int i = 0; i < tictactoeBoard.length; i++) 
		{
            for (int j = 0; j < tictactoeBoard[i].length; j++) 
			{
                tictactoeBoard[i][j] = Square.BLANK;
            }
        }
    }
	
	//Override the toString() method
    public String toString() 
	{
        String boardString = "";
        for (int i = 0; i < tictactoeBoard.length; i++) 
		{
            for (int j = 0; j < tictactoeBoard[i].length; j++) 
			{
                boardString += tictactoeBoard[i][j] + " ";
            }
            boardString += "\n";
        }
        return boardString;
    }
	
	// Place token method
    public boolean placeToken(int row, int col, Square playerToken) 
	{
		boolean answer = true;
        if (row < 0 || row > tictactoeBoard.length-1 || col < 0 || col > tictactoeBoard.length-1) 
		{
            answer = false;
        }
        else if (tictactoeBoard[row][col] == Square.BLANK) 
		{
            tictactoeBoard[row][col] = playerToken;
        }
        return answer;
    }

    // Check if full method
    public boolean checkIfFull() 
	{
		boolean fullOrNot = true;
        for (int i = 0; i < tictactoeBoard.length; i++) 
		{
            for (int j = 0; j < tictactoeBoard[i].length; j++) 
			{
                if (tictactoeBoard[i][j] == Square.BLANK) 
				{
                    fullOrNot = false;
                }
            }
        }
        return fullOrNot;
    }

    // Check if winning horizontal method
    private boolean checkIfWinningHorizontal(Square playerToken) 
	{
		boolean winningHoriz = false;
        for (int i = 0; i < tictactoeBoard.length; i++) 
		{
            if (tictactoeBoard[i][0] == playerToken && tictactoeBoard[i][1] == playerToken && tictactoeBoard[i][2] == playerToken) 
			{
                winningHoriz = true;
            }
        }
        return winningHoriz;
    }

    // Check if winning vertical method
    private boolean checkIfWinningVertical(Square playerToken) 
	{
		boolean winningVertic = false;
        for (int i = 0; i < tictactoeBoard.length; i++) 
		{
            if (tictactoeBoard[0][i] == playerToken && tictactoeBoard[tictactoeBoard.length-2][i] == playerToken && tictactoeBoard[tictactoeBoard.length-1][i] == playerToken)
			{
                winningVertic = true;
            }
        }
        return winningVertic;
    }

    // Check if winning method
    public boolean checkIfWinning(Square playerToken) 
	{
		boolean winning = false;
        if(checkIfWinningHorizontal(playerToken) || checkIfWinningVertical(playerToken))
		{
			winning = true;
		}
		return winning;
    }
	
	//bonus +_+_+
    private boolean checkIfWinningDiagonal(Square playerToken) 
	{
        // check the top-left to bottom-right diagonal
        boolean winDiagonal1 = true;
        for (int i = 0; i < tictactoeBoard.length; i++) 
		{
            if (tictactoeBoard[i][i] != playerToken) 
			{
                winDiagonal1 = false;
                break;
            }
        }
		
        if (winDiagonal1) 
		{
            return true;
        }
        // check the top-right to bottom-left diagonal
        boolean winDiagonal2 = true;
        for (int i = 0; i < tictactoeBoard.length; i++) 
		{
            if (tictactoeBoard[i][2-i] != playerToken) 
			{
                winDiagonal2 = false;
                break;
            }
        }
        return winDiagonal2;
	}
}
