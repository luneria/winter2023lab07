import java.util.Scanner;
public class TicTacToeGame 
{
    public static void main(String[] args) 
	{
        System.out.println("Welcome to Tic Tac Toe Game!");
		
        Board theBoard = new Board();
        boolean gameOver = false;
        int player = 1;
		
		Square playerToken = Square.X;

        System.out.println("Player 1's token: X");
		System.out.println("Player 2's token: O");
        while (!gameOver) 
		{
            System.out.println(theBoard);

            if (player == 1) 
			{
                playerToken = Square.X;
            } 
			else 
			{
                playerToken = Square.O;
            }
         
            System.out.println("Player " + player + ": it's your turn where do you want to place your token? ");
			Scanner scan = new Scanner(System.in);
            int row = scan.nextInt();
            int col = scan.nextInt();

            while (!theBoard.placeToken(row, col, playerToken)) 
			{
                System.out.println("Invalid move! Try again.");
                System.out.println("Player " + player + ": it's your turn where do you want to place your token? ");
                row = scan.nextInt();
                col = scan.nextInt();
            }

            if (theBoard.checkIfWinning(playerToken)) 
			{
                System.out.println("Player " + player + " wins!");
                gameOver = true;
            } 
			else if (theBoard.checkIfFull()) 
			{
                System.out.println("It's a tie!");
                gameOver = true;
            } 
			else 
			{
                player++;
                if (player > 2) 
				{
                    player = 1;
                }
            }
        }
    }
}